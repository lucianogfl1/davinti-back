package com.davinti.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.davinti.domain.Contato;
import com.davinti.services.ContatoService;

@RestController
@RequestMapping(value="/contatos")
public class ContatoResource {
	@Autowired
	private ContatoService service;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="{id}",method=RequestMethod.GET)
	public ResponseEntity<?> findById(@PathVariable Integer id) {
		Contato obj = service.findById(id);
		return ResponseEntity.ok().body(obj);
		
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method=RequestMethod.POST)
	public  ResponseEntity<Void> insert(@RequestBody Contato obj){		
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("{/id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
		
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="{id}",method=RequestMethod.PUT)
	public  ResponseEntity<Void> update(@RequestBody Contato obj, @PathVariable Integer id){		
		obj = service.update(obj);
		
		return ResponseEntity.noContent().build();
		
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="{id}",method=RequestMethod.DELETE)
	public  ResponseEntity<Void> delete(@PathVariable Integer id){		
		service.delete(id);		
		return ResponseEntity.noContent().build();		
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method=RequestMethod.GET)
	public  ResponseEntity<List<Contato>> findAll(){		
		List<Contato> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}

	
}
