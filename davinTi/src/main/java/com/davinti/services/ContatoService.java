package com.davinti.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davinti.domain.Contato;
import com.davinti.repositories.ContatoRepository;
import com.davinti.services.exceptions.ObjectNotFoundException;

@Service
public class ContatoService {
	@Autowired
	private ContatoRepository repo;
	public Contato findById(Integer id) {
		Optional<Contato> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				 "Objeto não encontrado! Id: " + id + ", Tipo: " + Contato.class.getName()));
	}
	
	
	public Contato insert(Contato obj) {
		obj.setId(null);
		return repo.save(obj);
	}
	
	public Contato update(Contato obj) {
		findById(obj.getId());
		return repo.save(obj);
	}
	public void delete(Integer id) {
		findById(id);
		repo.deleteById(id);				
	}
	public List<Contato> findAll() {
		return repo.findAll();
	}

}
