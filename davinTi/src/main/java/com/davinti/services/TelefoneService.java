package com.davinti.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.davinti.domain.Telefone;
import com.davinti.repositories.TelefoneRepository;
import com.davinti.services.exceptions.ObjectNotFoundException;

@Service
public class TelefoneService {
	@Autowired
	private TelefoneRepository repo;
	public Telefone findById(Integer id) {
		Optional<Telefone> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				 "Objeto não encontrado! Id: " + id + ", Tipo: " + Telefone.class.getName()));
	}
	
	public Telefone insert(Telefone obj) {
		obj.setId(2);
		return repo.save(obj);
	}

}
