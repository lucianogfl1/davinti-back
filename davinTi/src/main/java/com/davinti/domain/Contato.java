package com.davinti.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Contato implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private String nome;
	private Integer idade;
	private String telefone1;
	private String telefone2;
	private String telefone3;
	
	
	@OneToOne(mappedBy = "contato", cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Telefone telefone;

	public Contato() {

	}

	

	



	public Contato(Integer id, String nome, Integer idade, String telefone1, String telefone2, String telefone3,
			Telefone telefone) {
		super();
		this.id = id;
		this.nome = nome;
		this.idade = idade;
		this.telefone1 = telefone1;
		this.telefone2 = telefone2;
		this.telefone3 = telefone3;
		this.telefone = telefone;
	}







	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	public Telefone getTelefone() {
		return telefone;
	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	

	public String getTelefone1() {
		return telefone1;
	}



	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}







	public String getTelefone2() {
		return telefone2;
	}







	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}







	public String getTelefone3() {
		return telefone3;
	}







	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}







	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contato other = (Contato) obj;
		return Objects.equals(id, other.id);
	}
	

}
