package com.davinti.dto;

import java.io.Serializable;

public class ContatoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private Integer idade;
	private String telefone1;
	private String telefone2;
	private String telefone3;
	private Integer contatoId;
	
	
	public ContatoDTO() {
		
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public String getTelefone3() {
		return telefone3;
	}
	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}
	public Integer getContatoId() {
		return contatoId;
	}
	public void setContatoId(Integer contatoId) {
		this.contatoId = contatoId;
	}
	

}
