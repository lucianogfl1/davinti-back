package com.davinti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DavinTiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DavinTiApplication.class, args);
	}

}
