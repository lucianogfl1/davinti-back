package com.davinti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.davinti.domain.Telefone;


@Repository
public interface TelefoneRepository  extends JpaRepository<Telefone, Integer> {

}
